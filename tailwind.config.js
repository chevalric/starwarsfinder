const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  theme: {
    boxShadow: {
      ...defaultTheme.boxShadow,
      outline: '0 0 0 3px rgba(153, 153, 153, 0.5)'
    }
  },
  variants: {},
  plugins: []
};
