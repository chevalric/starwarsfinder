export const actions = {
  CHANGE_FILM: 'CHANGE_FILM',
  CHANGE_SPECIES: 'CHANGE_SPECIES',
  CHANGE_PERSON: 'CHANGE_PERSON',
  GET_NEXT_PAGE: 'GET_NEXT_PAGE',
  GET_PREV_PAGE: 'GET_PREV_PAGE',
};

function getFilmFilter(state, film) {
  if (film) {
    return {
      ...state.filters,
      speciesFilter: {
        films_some: {
          id: film
        }
      },
      personFilter: {
        AND: {
          films_some: {
            id: film
          }
        }
      }
    }
  } else {
    return {
      ...state.filters,
      speciesFilter: {},
      personFilter: {}
    }
  }
}

function getSpeciesFilter(state, species) {
  if (species) {
    const personFilter = {
      ...state.filters.personFilter || {},
      AND: {
        films_some: state.selectedFilm ? {
          id: state.selectedFilm
        } : {},
        species_some: {
          id: species
        }
      }
    };
    return {
      ...state.filters,
      personFilter: {
        ...state.filters.personFilter,
        ...personFilter
      }
    }
  } else {
    const filmFilter = {
      AND: {
        films_some: state.selectedFilm ? {
          id: state.selectedFilm
        } : {},
      }
    };
    return {
      ...state.filters,
      speciesFilter: filmFilter,
      personFilter: {
        ...state.filters.personFilter,
        ...filmFilter
      }
    }
  }
}

function getPersonFilter(state, person) {
  const otherFilters = {
    AND: {
      films_some: state.selectedFilm ? {
        id: state.selectedFilm
      } : {},
      species_some: state.selectedSpecies ? {
        id: state.selectedSpecies
      } : {}
    }
  };
  if (person) {
    return {
      ...state.filters,
      personFilter: {
        // id: person,
        ...otherFilters
      }
    }
  } else {
    return {
      ...state.filters,
      personFilter: otherFilters
    }
  }
}

function getCursor(data: Array<{ id: string }>, first: boolean = false) {
  if (first) {
    return data[0].id;
  }
  const lastItem = data[data.length - 1];
  if (lastItem) {
    return lastItem.id;
  }
}

export type Action = {
  type: string
  payload: any
}

export function starWarsReducer(state, action: Action) {
  switch (action.type) {
    case actions.GET_NEXT_PAGE:
      return {
        ...state,
        cursor: getCursor(action.payload),
        direction: 'after',
        first: 10,
        last: null
      };
    case actions.GET_PREV_PAGE:
      return {
        ...state,
        cursor: getCursor(action.payload, true),
        direction: 'before',
        last: 10,
        first: null
      };
    case actions.CHANGE_FILM:
      return {
        ...state,
        cursor: null,
        selectedFilm: action.payload,
        filters: getFilmFilter(state, action.payload)
      };
    case actions.CHANGE_SPECIES:
      return {
        ...state,
        cursor: null,
        selectedSpecies: action.payload,
        filters: getSpeciesFilter(state, action.payload)
      };
    case actions.CHANGE_PERSON:
      return {
        ...state,
        cursor: null,
        selectedPerson: action.payload,
        filters: getPersonFilter(state, action.payload)
      };
  }
}
