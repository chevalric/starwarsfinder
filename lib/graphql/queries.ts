import gql from 'graphql-tag';

export const FiltersQuery = gql`
  query GET_FILTER_LISTS($speciesFilter:SpeciesFilter, $personFilter:PersonFilter) {
    allFilms(orderBy: title_ASC) {
      id
      title
    }
    allSpecies(filter:$speciesFilter, orderBy: name_ASC) {
      id
      name
    }
    allPersons(filter:$personFilter,orderBy: name_ASC) {
      id
      name
    }
  }`;

export const PersonsQuery = gql`
  query GET_PERSON_DATA($personFilter: PersonFilter, $after: String, $before: String, $first: Int, $last: Int){
    allPersons(filter:$personFilter, orderBy: name_ASC, first: $first, last: $last, after: $after, before: $before) {
      id
      name
      species {
        name
      }
      birthYear
      hairColor
      eyeColor
      skinColor
      height
      mass
      homeworld {
        name
      }
      films {
        title
      }
    }
  }`;
