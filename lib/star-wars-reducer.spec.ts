import {starWarsReducer, actions} from './star-wars-reducer';

describe('Reducer: Star Wars', () => {
  const initialState = {
    filters: {}
  };
  it('should create filters for species and persons when a film is selected', () => {
    const filmId = '12345';
    const state = starWarsReducer(initialState, {type: actions.CHANGE_FILM, payload: filmId});
    expect(state).toEqual(expect.objectContaining({
      selectedFilm: filmId,
      filters: {
        personFilter: {
          AND: {
            films_some: {
              id: filmId
            }
          }
        },
        speciesFilter: {
          films_some: {
            id: filmId
          }
        }
      }
    }))
  });

  it('should clear the filters if a film is deselected', () => {
    const filmId = '12345';
    const initialStateWithFilmFilter = {
      selectedFilm: filmId,
      filters: {
        personFilter: {
          AND: {
            films_some: {
              id: filmId
            }
          }
        },
        speciesFilter: {
          films_some: {
            id: filmId
          }
        }
      }
    };
    const newState = starWarsReducer(initialStateWithFilmFilter, {type: actions.CHANGE_FILM, payload: ''});
    expect(newState).toEqual(expect.objectContaining({
      selectedFilm: '',
      filters: {
        personFilter: {},
        speciesFilter: {}
      }
    }))
  });

  it('should create filters for persons when a species is selected', () => {
    const speciesId = '12345';
    const state = starWarsReducer(initialState, {type: actions.CHANGE_SPECIES, payload: speciesId});
    expect(state).toEqual(expect.objectContaining({
      selectedSpecies: speciesId,
      filters: {
        personFilter: {
          AND: {
            films_some: {},
            species_some: {
              id: speciesId
            }
          }
        },
      }
    }))
  });

  it('should delete the filters for persons when a species is deselected', () => {
    const speciesId = '12345';
    const initialStateWithSpeciesFilter = {
      selectedSpecies: speciesId,
      filters: {
        personFilter: {
          AND: {
            films_some: {},
            species_some: {
              id: speciesId
            }
          }
        },
      }
    };
    const state = starWarsReducer(initialStateWithSpeciesFilter, {type: actions.CHANGE_SPECIES, payload: ''});
    expect(state).toEqual(expect.objectContaining({
      selectedSpecies: '',
      filters: {
        personFilter: {
          AND: {
            films_some: {}
          }
        },
        speciesFilter: {
          AND: {
            films_some: {}
          }
        }
      }
    }))
  });

  it('should create filters for persons when a person is selected', () => {
    const personId = '12345';
    const state = starWarsReducer(initialState, {type: actions.CHANGE_PERSON, payload: personId});
    expect(state).toEqual(expect.objectContaining({
      selectedPerson: personId,
      filters: {
        personFilter: {
          AND: {
            films_some: {},
            species_some: {}
          }
        },
      }
    }))
  });

  it('should delete filters for persons when a person is deselected', () => {
    const personId = '12345';
    const stateWithPersonFilter = {
      selectedPerson: personId,
      filters: {
        personFilter: {
          id: personId,
          AND: {
            films_some: {},
            species_some: {}
          }
        },
      }
    };
    const state = starWarsReducer(stateWithPersonFilter, { type: actions.CHANGE_PERSON, payload: '' });
    expect(state).toEqual(expect.objectContaining({
      selectedPerson: '',
      filters: {
        personFilter: {
          AND: {
            films_some: {},
            species_some: {},
          }
        }
      }
    }))
  });

  it('should create filters for persons when a species and a film are selected', () => {
    const speciesId = '12345';
    const filmId = '67890';

    const state = starWarsReducer(initialState, {type: actions.CHANGE_FILM, payload: filmId});
    const nextState = starWarsReducer(state, {type: actions.CHANGE_SPECIES, payload: speciesId});

    expect(nextState).toEqual(expect.objectContaining({
        filters: {
          speciesFilter: {
            films_some: {
              id: filmId
            }
          },
          personFilter: {
            AND: {
              films_some: {
                id: filmId,
              },
              species_some: {
                id: speciesId
              }
            }
          }
        }
      })
    );
  });
});
