/*
 Because Tailwind generates a huge blob of CSS, it's advised to
 use purgecss to remove any unused css. This works in a really simple way:
 it scans all files for class names and then removes all class names that
 weren't found.
 */
const purgecss = require('@fullhuman/postcss-purgecss')({
  content: [
    './pages/**/*.tsx',
    './components/**/*.tsx',
  ],

  defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || [],
});

module.exports = {
  plugins: [
    require('tailwindcss'),
    require('autoprefixer'),
    // Use purgeCSS only in production, so that there are no missing classes
    // during development
    ...(process.env.NODE_ENV === 'production' ? [purgecss] : []),
  ],
};
