import React, {Fragment, useReducer} from 'react';
import {useQuery} from "@apollo/react-hooks";

import './index.css';
import {withApollo} from "../lib/graphql/init";
import {FiltersQuery, PersonsQuery} from "../lib/graphql/queries";
import {starWarsReducer, actions} from "../lib/star-wars-reducer";
import {Filter} from '../components/Filter';
import {PersonList} from "../components/PersonList";
import {Pager} from "../components/Pager";

/**
 * This is the entry point for the site. NextJS uses the pages directory
 * as the basis for its routing.
 */
function Index() {
  /*
    because there's a lot of state, a reducer is a better solution than
    many separate useState calls.
   */
  const [state, dispatch] = useReducer(starWarsReducer, {
    cursor: null,
    direction: 'after',
    first: 10,
    last: null,
    selectedFilm: '',
    selectedSpecies: '',
    selectedPerson: '',
    filters: {}
  });

  /*
    This first query gets the data for the three dropdown at the top of
    the page. It also filters the data based on previous selections
   */
  const {loading, error, data} = useQuery(FiltersQuery, {
    variables: {
      speciesFilter: state.filters.speciesFilter,
      personFilter: state.filters.personFilter
    }
  });

  /*
    For the second query the person filter needs to be expanded
    with a filter when a person is selected in the person dropdown
   */
  const personFilter = {
    ...state.filters.personFilter
  };
  if (state.selectedPerson) {
    personFilter.id = state.selectedPerson;
  }

  /*
    The second query retrieves data for persons. This data is later used
    to show some information on the back of the cards in the list, which is
    revealed by hovering or focusing the card.
   */
  const personListData = useQuery(PersonsQuery, {
    variables: {
      personFilter,
      [state.direction]: state.cursor,
      first: state.first,
      last: state.last
    },
  });

  if (loading) {
    return <p>Loading...</p>
  }

  if (error) {
    return <p>Error: {error.message}</p>
  }

  const handleChange = type => event => {
    event.preventDefault();
    dispatch({
      type,
      payload: event.target.value
    });
  };

  return (
    <Fragment>
      <h1 className="mb-8 text-2xl bg-indigo-500 text-yellow-300 text-center shadow border-b border-indigo-600">Star Wars finder</h1>
      <div className="container mx-auto">
        <form className="mb-8 flex flex-col md:flex-row justify-between">
          <Filter
            placeholder="Select a movie"
            id="film"
            onChange={handleChange(actions.CHANGE_FILM)}
            value={state.selectedFilm}
            options={data.allFilms.map(({title, id}) => ({label: title, value: id}))}
          />
          <Filter
            placeholder="Select a species"
            id="film"
            onChange={handleChange(actions.CHANGE_SPECIES)}
            value={state.selectedSpecies}
            options={data.allSpecies.map(({name, id}) => ({label: name, value: id}))}
          />
          <Filter
            placeholder="Select a person"
            id="film"
            onChange={handleChange(actions.CHANGE_PERSON)}
            value={state.selectedPerson}
            options={data.allPersons.map(({name, id}) => ({label: name, value: id}))}
          />
        </form>
        <PersonList data={personListData} />
        <Pager
          pageData={personListData.data.allPersons}
          allPersons={data.allPersons}
          dispatch={dispatch}
        />
      </div>
    </Fragment>
  );
}

export default withApollo(Index)
