declare type Species = {
  name: string
  id: string
}

declare type Film = {
  title: string
  id: string
}

declare type Person = {
  id: string
  name: string
  species: Species
  birthYear: number
  hairColor: string
  eyeColor: string
  skinColor: string
  height: number
  mass: number
  films: Film[]
  __typename: string
}
