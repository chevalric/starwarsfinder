import {HeroCard} from "../HeroCard";
import React from "react";
import {QueryResult} from "@apollo/react-common/lib/types/types";

type PersonQueryResult = {
  allPersons: Person[]
}

type PersonListProps = {
  data: QueryResult<PersonQueryResult>
}

export function PersonList({ data }: PersonListProps ) {
  const { loading, error, data: { allPersons } } = data;

  if (loading) {
    return <p>Loading...</p>
  }

  if (error) {
    return <p>Error: {error.message}</p>
  }

  return (
    <ol className="grid gap-xl grid-repeat-1 md:grid-repeat-5">
      {allPersons.map((person) => {
        const { name, id } = person;
        return (
          <HeroCard
            key={id}
            name={name}
            details={person}
          />
        );
      })}
    </ol>
  )
}
