import React from 'react';
import { render } from '@testing-library/react';

import { HeroCard } from './index';

describe('Component: HeroCard', () => {
  const Luke = {
    id: '1',
    name: 'Luke Skywalker',
    birthYear: 30,
    hairColor: 'BLONDE',
    eyeColor: 'BLUE',
    skinColor: 'FAIR',
    height: 160,
    mass: 90,
    homeworld: {
      name: 'Tatooine'
    },
    films: [ { id: '3', title: 'A New Hope' } ],
    species: { id: '2', name: 'Human' },
    __typename: 'Person'
  };

  it('should display a Star Wars character', () => {
    const props = {
      name: 'Luke Skywalker',
      movies: ['A New Hope', 'Return of the Jedi', 'Empire Strikes Back', 'The Last Jedi', 'The Force Awakens'],
      details: Luke
    };

    const { getAllByText, getByText, getByAltText } = render(<HeroCard {...props} />);

    expect(getAllByText(props.name)[0]).toBeVisible();
    expect(getByText(props.movies.join(', '))).toBeVisible();
    expect(getByAltText(`Star Wars logo`)).toBeVisible();
  });

  it('should display a placeholder image if no image url is provided', () => {
    const props = {
      name: 'Luke Skywalker',
      movies: ['A New Hope', 'Return of the Jedi', 'Empire Strikes Back', 'The Last Jedi', 'The Force Awakens'],
      details: Luke
    };

    const { getByAltText } = render(<HeroCard {...props} />);

    const img: HTMLImageElement = getByAltText('Star Wars logo') as HTMLImageElement;
    expect(img).toBeVisible();
    expect(img.src).toContain('sw-logo.gif');
  });
});
