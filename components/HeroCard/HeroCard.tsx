import React, {Fragment} from 'react';

import './HeroCard.css';

/*
  Because the incoming data can be quite varied (a string, an array of strings or an array of objects)
  some normalization is needed.
 */
function normalizeValue(value) {
  if (Array.isArray(value)) {
    if (value.length === 1) {
      if (typeof value[0] === 'string') {
        return <span className="w-full">{value[0].toLowerCase()}</span>
      } else {
        return <span className="w-full">{value[0].name || value[0].title}</span>
      }
    }
    return (
      <ul className="w-full">{
        value.map(({title = null, name = null}) => (
          <li key={title || name}>{title || name}</li>
        ))
      }</ul>
    );
  } else if (typeof value === 'object' && value.name) {
    return (<span className="w-full">{value.name}</span>);
  }
  return (<span className="w-full">{value}</span>);
}

type HeroCardProps = {
  name: string
  movies?: string[]
  details?: Person
}

export function HeroCard({
                           name,
                           movies,
                           // extract id and typename from the object, so they don't show up on the card
                           details: {
                             id,
                             __typename,
                             ...details
                           }
                         }: HeroCardProps) {
  // The li has a tabindex so it can be focused with the keyboard. This allows a keyboard user to tab
  // through the list and see the back of the cards.
  return (
    <li className="card-container h-64 mb-8 md:mb-0 w-48 md:w-48 focus:outline-none focus:shadow-outline" tabIndex={0}>
      <div className="card">
        <div className="front h-64 z-up shadow-md border rounded flex flex-col self-center">
          <img
            src='sw-logo.gif'
            alt="Star Wars logo"
            className="flex-none md:max-w-48"
          />
          <p className="m-4 text-center">
            <span className="block text-lg">{name}</span>
            {movies && <span className="block text-gray-500 italic">{movies.join(', ')}</span>}
          </p>
        </div>
        <dl className="back h-64 overflow-y-auto w-48 py-2 px-4 shadow-md border rounded self-center">
          {details && Object.entries(details).map(([key, value]) => {
              if (!value) return null;
              return (
                <Fragment key={key}>
                  <dt className="w-full font-bold">{key}</dt>
                  <dd>
                    {normalizeValue(value)}
                  </dd>
                </Fragment>
              );
            }
          )}
        </dl>
      </div>
    </li>
  );
}
