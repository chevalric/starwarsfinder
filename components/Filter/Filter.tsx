import React, {ChangeEventHandler} from "react";

type Option = {
  label: string
  value: string
}

type FilterProps = {
  placeholder: string
  id: string
  value: string
  options: Option[]
  onChange: ChangeEventHandler
}

export function Filter ({ placeholder, id, value, options, onChange }: FilterProps) {
  return (
    <select className="mb-2 px-2 py-8 border focus:outline-none focus:shadow-outline" id={id} onChange={onChange} value={value}>
      <option value="">{placeholder}</option>
      {options.map(({label, value}) => <option key={value} value={value}>{label}</option>)}
    </select>

  )
}
