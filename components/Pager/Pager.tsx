import React, {Fragment} from 'react';
import {actions, Action} from "../../lib/star-wars-reducer";

type Person = {
  id: string
  name: string
}

type PagerProps = {
  pageData: Person[]
  allPersons: Person[]
  dispatch: (action: Action) => void
}

/**
 * The Pager displays the previous page and next page buttons. When there is no previous or next page,
 * the button is hidden.
 *
 * @param pageData the items on the current page
 * @param allPersons the entire list of items
 * @param dispatch the dispatch function for the main reducer
 * @constructor
 */
export function Pager({pageData, allPersons, dispatch }: PagerProps) {
  // To see if the previous page button needs to be shown, a check is done to see if the
  // first item on the page is in the first 10 items of the entire list.,
  const hasPrevPage = () => {
    const cursorIndex = allPersons.findIndex(({id}) => id === pageData[0].id);
    return cursorIndex - 10 >= -1;
  };

  // To see if the next page button needs to be shown, a check is done to see if the
  // last item on the page is in the last 10 items of the entire list.
  // Also, if the page has less than 10 items, the button is not shown.
  const hasNextPage = () => {
    if(pageData.length < 10) {
      return false;
    }
    const cursorIndex = allPersons.findIndex(({id}) => id === pageData[pageData.length - 1].id);
    return cursorIndex <= allPersons.length;
  };

  const getPrevPage = () => {
    dispatch({type: actions.GET_PREV_PAGE, payload: pageData});
  };

  const getNextPage = () => {
    dispatch({type: actions.GET_NEXT_PAGE, payload: pageData});
  };

  return (
    <Fragment>
      {hasPrevPage() && <button
        className="m-4 ml-0 px-2 py-1 border text-white bg-indigo-500 rounded border border-indigo-600 shadow focus:outline-none focus:shadow-outline"
        onClick={getPrevPage}
      >
        Previous page
      </button>}
      {hasNextPage() && <button
        className="m-4 ml-0 px-2 py-1 border text-white bg-indigo-500 rounded border border-indigo-600 shadow focus:outline-none focus:shadow-outline"
        onClick={getNextPage}
      >
        Next page
      </button>}
    </Fragment>
  )
}
